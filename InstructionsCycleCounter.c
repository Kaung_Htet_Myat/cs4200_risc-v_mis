#include <stdio.h>
#include <clocks.h>
#define INLINE_ELAPSED(INL) static INL double elapsed(ticks t1, ticks t0) \
{									  \
     return (double)t1 - (double)t0;					  \
}
__attribute__ ((__noinline__))
void * get_pc () { return __builtin_return_address(0); }


int main () {
	typedef unsigned long ticks;
    printf("%p\n", get_pc());
    printf("%p\n", get_pc());
    printf("%p\n", get_pc());
    ticks getticks(void);
    printf("%p\n");
    return 0;
}
