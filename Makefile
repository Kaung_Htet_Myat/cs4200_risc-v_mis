all: mas

mas: cpu.c cpu.h memory.c memory.h decision-helper.c decision-helper.o syscall.c syscall.h InstructionCycleCounter.c clocks.h pipeline.c single-cycle.c
	gcc -O2 cpu.c memory.c decision-helper.c syscall.c  InstructionsCycleCounter.c pipeline.c single-cycle.c -o mas

clean:
	-rm mas single-cycle-mis.mxe
