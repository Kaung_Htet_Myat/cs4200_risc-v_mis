/*
 * Copyright (C) 2018 Gedare Bloom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu.h"
#include "memory.h"
#include "encode.c"
#include "decision-helper.c"

struct cpu_context cpu_ctx;

int fetch(struct IF_ID_buffer *out)
{
	*out.next_pc = nextPc_adder(*out.next_pc);
	return 0;
}

int decode(struct IF_ID_buffer *in, struct ID_EX_buffer *out)
{
	*out.opcode = get_opcode(*in);
	*out.rs = get_reg(*in);
	*out.rt = get_reg(*in);
	*out.instruction = *in.instruction;
	*out.pc = *in.pc;
	f7 = get_reg(*in);
	out.f7 = f7;
	*out.next_pc = *in.next_pc;
}

int execute(struct ID_EX_buffer *in, struct EX_MEM_buffer *out)
{
	if (*in.f7 == 0x0)
	{
		*out.ALU_result = addALU(*in.rs, *in.rt);
	}
	elif (*in.f7 == 0x20)
	{
		*out.ALU_result = subtractALU(*in.rs, *in.rt);
	}

	if (*in.opcode == 0x03)
	{
		*out.readRegister = *in.rs;
		*out.writeRegister = 0;
	}
	elif (*in.opcode == 0x23)
	{
		*out.writeRegister = *in.rs;
		*out.readRegister = 0;
	}
	elif (*in.opcode == 0x6F)
	{
		uint32_t immediate = get_imm(*in_instruction);
		*out.next_pc = immediate;
	}
	elif (*in.opcode == 0x63)
	{
		flag = controlcompareSignal(*in.rs, *in.rt);
		if (flag == 1)
		{
			*out.next_pc = get_imm(*in_instruction);
		}
		else
		{
			*out.next_pc = *in.next_pc;
		}
	}
	return 0;
}

int memory(struct EX_MEM_buffer *in, struct MEM_WB_buffer *out)
{
	if (*in.readRegister != 0)
	{
		*in.addresstoAccess = get_imm(*in_instruction);
		*in.rs = load(*out.addresstoAccess);
	}
	elif (*in.writeRegister != 0)
	{
		*int.addresstoAccess = get_imm(*in_instruction);
		*out.rs = load(*out.addresstoAccess);
	}
	return 0;
}

int writeback(struct MEM_WB_buffer *in)
{
	*in.writeRegister = *in.writeData;
	return 0;
}
